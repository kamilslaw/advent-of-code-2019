﻿using System;
using System.IO;
using System.Linq;

namespace day01
{
    class Program
    {
        static void Main()
        {
            var result = File.ReadAllLines("input.txt")
                .Select(int.Parse)
                .Select(GetMass)
                .Sum();
            Console.WriteLine(result);
        }

        private static int GetMass(int value)
        {
            var fuel = value / 3 - 2;
            if (fuel <= 0) return 0;
            return fuel + GetMass(fuel);
        }
    }
}
