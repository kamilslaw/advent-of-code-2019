﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _23
{
    class Program
    {
        static void Main()
        {
            var codes = System.IO.File.ReadAllText("input.txt")
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 1_000).Select(_ => 0L))
                .ToList();

            _ = Task.Factory.StartNew(async () => await new NAT(codes).Run(), TaskCreationOptions.LongRunning); // PART 2

            var tasks = Enumerable.Range(0, 50)
                .Select(n => new NIC(n, codes))
                .ToList()
                .Select(nic => Task.Factory.StartNew(nic.Run, TaskCreationOptions.LongRunning))
                .ToArray();
            Task.WaitAll(tasks);
        }
    }

    class NAT // PART 2
    {
        private static (long X, long Y)? _packet;
        private static object lockObj = new object();
        private List<long> codes;

        public NAT(List<long> codes)
        {
            this.codes = codes.ToList();
        }

        public static void Receive((long X, long Y) packet)
        {
            lock (lockObj)
            {
                _packet = packet;
            }
        }

        public async Task Run()
        {
            _ = Task.Factory.StartNew(() => NIC.Loop(codes, () => 255, _ => throw new AccessViolationException()), TaskCreationOptions.LongRunning);
            while (true)
            {
                await Task.Delay(100);
                if (NIC.instances.All(i => i != null && i.queue.IsEmpty))
                {
                    await Task.Delay(100);
                    lock (lockObj)
                    {
                        if (NIC.instances.All(i => i.queue.IsEmpty) && _packet.HasValue)
                        {
                            Console.WriteLine($"[NAT] -> [00] ({_packet})");
                            NIC.instances[0].queue.Enqueue(_packet.Value);
                        }
                    }
                }
            }
        }
    }

    class NIC
    {
        public static NIC[] instances = new NIC[50];
        public ConcurrentQueue<(long X, long Y)> queue = new ConcurrentQueue<(long X, long Y)>();
        private int n;
        private List<long> codes;

        public NIC(int n, List<long> codes)
        {
            this.n = n;
            this.codes = codes.ToList();
            instances[n] = this;
        }

        public void Run()
        {
            var initialized = false;

            var packet = default((long X, long Y)?);

            var outputN = default(int?);
            var outputX = default(long?);

            Loop(
                codes,
                () =>
                {
                    var value = -1L;
                    if (!initialized)
                    {
                        value = n;
                        initialized = true;
                    }
                    else if (packet.HasValue)
                    {
                        value = packet.Value.Y;
                        packet = null;
                    }
                    else if (queue.TryDequeue(out var p))
                    {
                        value = p.X;
                        packet = p;
                    }
                    return value;
                },
                result =>
                {
                    if (!outputN.HasValue) outputN = (int)result;
                    else if (!outputX.HasValue) outputX = result;
                    else
                    {
                        Console.WriteLine($"[{n.ToString().PadLeft(2, '0')}] -> [{outputN.ToString().PadLeft(2, '0')}] ({outputX}, {result})");
                        if (outputN.Value != 255) instances[outputN.Value].queue.Enqueue((outputX.Value, result));
                        else NAT.Receive((outputX.Value, result)); // PART 2
                        outputN = null;
                        outputX = null;
                    }
                });
        }

        public static void Loop(List<long> codes, Func<long> getInput, Action<long> proceedOutput)
        {
            var pos = 0;
            var relativeBase = 0;

            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3 || codes[pos] == 203)
                {
                    var index = codes[pos] == 3 ? (int)codes[pos + 1] : (int)(relativeBase + codes[pos + 1]);
                    codes[index] = getInput();
                    pos += 2;
                }
                else if (codes[pos] == 4 || codes[pos] == 104 || codes[pos] == 204)
                {
                    var result = codes[pos] == 4 ? codes[(int)codes[pos + 1]] : codes[pos] == 104 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];
                    pos += 2;
                    proceedOutput(result);                    
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }
        }
    }
}
