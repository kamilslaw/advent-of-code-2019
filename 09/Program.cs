﻿using System;
using System.Linq;

namespace day09
{
    class Program
    {
        static void Main()
        {
            var codes =
                System.IO.File.ReadAllText("input.txt")
                // "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
                // "1102,34915192,34915192,7,4,7,99,0"
                // "104,1125899906842624,99"
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 10_000).Select(_ => 0L))
                .ToList();

            long output = 2;
            var pos = 0;
            var relativeBase = 0;

            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3)
                {
                    codes[(int)codes[pos + 1]] = output;
                    pos += 2;
                }
                else if (codes[pos] == 203)
                {
                    codes[(int)(relativeBase + codes[pos + 1])] = output;
                    pos += 2;
                }
                else if (codes[pos] == 4)
                {
                    output = codes[(int)codes[pos + 1]];
                    pos += 2;
                    Console.WriteLine(output);
                }
                else if (codes[pos] == 104)
                {
                    output = codes[pos + 1];
                    pos += 2;
                    Console.WriteLine(output);
                }
                else if (codes[pos] == 204)
                {
                    output = codes[(int)(relativeBase + codes[pos + 1])];
                    pos += 2;
                    Console.WriteLine(output);
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }
        }
    }
}
