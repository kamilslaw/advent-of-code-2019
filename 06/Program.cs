﻿using System;
using System.Collections.Generic;

namespace day06
{
    class Pos
    {
        public string val;
        public Pos prev;
    }

    class Program
    {
        static void Main(string[] args)
        {
            var lines = System.IO.File.ReadAllLines("input.txt");
            // var lines = System.IO.File.ReadAllLines("inputTest.txt");
            // var lines = System.IO.File.ReadAllLines("inputTest2.txt");
            var values = new Dictionary<string, Pos>();

            foreach (var line in lines)
            {
                var parts = line.Split(')');
                Pos left = null;
                if (values.ContainsKey(parts[0])) left = values[parts[0]];
                else
                {
                    left = new Pos { val = parts[0] };
                    values.Add(parts[0], left);
                }

                if (!values.ContainsKey(parts[1])) values.Add(parts[1], new Pos { val = parts[1] });
                values[parts[1]].prev = left;
            }

            var result = 0;
            foreach (var pos in values.Values)
            {
                var tmp = pos;
                while (tmp.prev != null)
                {
                    tmp = tmp.prev;
                    result += 1;
                }
            }

            Console.WriteLine(result);

            var youChain = GetChain(values["YOU"]);
            var sanChain = GetChain(values["SAN"]);
            for (int y = 0; y < youChain.Count; y++)
            {
                for (int s = 0; s < sanChain.Count; s++)
                {
                    if (youChain[y] == sanChain[s])
                    {
                        Console.WriteLine(y + s);
                        goto end;
                    }
                }
            }
        end:;
        }

        private static List<string> GetChain(Pos pos)
        {
            var result = new List<string>();
            var tmp = pos.prev;
            while (tmp != null)
            {
                result.Add(tmp.val);
                tmp = tmp.prev;
            }

            return result;
        }
    }
}
