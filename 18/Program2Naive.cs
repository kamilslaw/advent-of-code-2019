﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day18_2naive
{
    class Program
    {
        static void Main()
        {
            var input = System.IO.File.ReadAllLines("input2.txt");
            // var input = System.IO.File.ReadAllLines("input2_8.txt");
            // var input = System.IO.File.ReadAllLines("input2_24.txt");
            // var input = System.IO.File.ReadAllLines("input2_72.txt");

            // Split on 4 inputs, & run part 1 method for each of them, then sum value
            var X = (int)Math.Ceiling(input[0].Length / 2.0);
            var Y = (int)Math.Ceiling(input.Length / 2.0);
            day18.Program.Perform(Prepare(input.Select(l => l.Take(X).ToList()).Take(Y).ToList()));
            day18.Program.Perform(Prepare(input.Select(l => l.Skip(X - 1).ToList()).Take(Y).ToList()));
            day18.Program.Perform(Prepare(input.Select(l => l.Take(X).ToList()).Skip(Y - 1).ToList()));
            day18.Program.Perform(Prepare(input.Select(l => l.Skip(X - 1).ToList()).Skip(Y - 1).ToList()));
        }

        private static string[] Prepare(List<List<char>> input)
        {
            var bigLetters = new HashSet<char>();
            for (int y = 0; y < input.Count; y++)
                for (int x = 0; x < input[0].Count; x++)
                    if (input[y][x] >= 'a' && input[y][x] <= 'z') bigLetters.Add((char)(input[y][x] - 32));
            for (int y = 0; y < input.Count; y++)
                for (int x = 0; x < input[0].Count; x++)
                    if (input[y][x] >= 'A' && input[y][x] <= 'Z' && !bigLetters.Contains(input[y][x])) input[y][x] = '.';
            return input.Select(l => string.Join("", l)).ToArray();
        }
    }
}
 