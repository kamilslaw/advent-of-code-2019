﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace day18
{
    class Program
    {
        static List<List<int>> _costs;
        static Dictionary<(int hash, int x, int y), int> _checked;
        static List<bool> _letterChecked = Enumerable.Repeat(false, 26).ToList();

        static void Main2()
        {
            var input = System.IO.File.ReadAllLines("input.txt");
            // var input = System.IO.File.ReadAllLines("input_8.txt");
            // var input = System.IO.File.ReadAllLines("input_86.txt");
            // var input = System.IO.File.ReadAllLines("input_132.txt");
            // var input = System.IO.File.ReadAllLines("input_136.txt");
            // var input = System.IO.File.ReadAllLines("input_81.txt");
            Perform(input);
        }

        public static void Perform(string[] input)
        {
            _checked = new Dictionary<(int, int, int), int>();

            var map = input.Select(l => l.Select(c => c == '.' ? '|' : c).ToList()).ToList();
            var X = input[0].Length;
            var Y = input.Length;

            _costs = Enumerable.Range(0, Y).Select(_ => Enumerable.Repeat(int.MaxValue, X).ToList()).ToList();

            var (x, y) = GetStart(map, X, Y);
            map[y][x] = '|';

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var steps = 0;
            var length = GetPathLength(map, X, Y, x, y, 0, ref steps);
            stopwatch.Stop();

            Console.WriteLine("Result: " + length);
            Console.WriteLine("Steps:  " + steps);
            Console.WriteLine("Time:   " + stopwatch.ElapsedMilliseconds + "ms");
        }

        static int GetPathLength(List<List<char>> map, int X, int Y, int x, int y, int depth, ref int steps, int totalCost = 0)
        {
            if (IsEmpty(map, X, Y)) return 0;
            steps += 1;
            var min = int.MaxValue;
            var options = GetPossibilities(map, X, Y, x, y);

            foreach (var v in options)
            {
                var bigLetter = map[v.y][v.x] - 32;
                for (int i = 'a'; i <= 'z'; i++) _letterChecked[i - 'a'] = true;

                (int x, int y)? toChange = null;
                for (int j = 0; j < Y; j++) for (int i = 0; i < X; i++)
                    {
                        if (map[j][i] == bigLetter) toChange = (i, j);
                        var letter = map[j][i];
                        if ((j != v.y || i != v.x) && letter >= 'a' && letter <= 'z') _letterChecked[letter - 'a'] = false;
                    }

                var key = (GetLettersHash(), v.x, v.y);
                if (_letterChecked.Any(c => !c) && _checked.ContainsKey(key) && _checked[key] < totalCost + v.cost) continue;
                else
                {
                    if (!_checked.ContainsKey(key)) _checked.Add(key, totalCost + v.cost);
                    else _checked[key] = totalCost + v.cost;
                }

                var newMap = Clone(map);
                newMap[v.y][v.x] = '|';
                if (toChange.HasValue) newMap[toChange.Value.y][toChange.Value.x] = '|';

                var newMin = v.cost + GetPathLength(newMap, X, Y, v.x, v.y, depth + 1, ref steps, totalCost + v.cost);
                if (newMin < min && newMin > 0) min = newMin;
            }

            return min;
        }

        private static IReadOnlyList<(int x, int y, int cost)> GetPossibilities(List<List<char>> map, int X, int Y, int x, int y)
        {
            var result = new List<(int x, int y, int cost)>();
            for (int j = 0; j < Y; j++) for (int i = 0; i < X; i++) _costs[j][i] = int.MaxValue;
            _costs[y][x] = 0;
            var toVisit = new Queue<(int x, int y)>();
            toVisit.Enqueue((x, y));
            while (toVisit.Count > 0)
            {
                var t = toVisit.Dequeue();
                var c = map[t.y][t.x];
                if (c == '|')
                {
                    if (t.y > 0 && _costs[t.y - 1][t.x] > _costs[t.y][t.x] + 1 && map[t.y - 1][t.x] > 'Z')
                    {
                        _costs[t.y - 1][t.x] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.x, t.y - 1));
                    }
                    if (t.y < Y - 1 && _costs[t.y + 1][t.x] > _costs[t.y][t.x] + 1 && map[t.y + 1][t.x] > 'Z')
                    {
                        _costs[t.y + 1][t.x] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.x, t.y + 1));
                    }
                    if (t.x > 0 && _costs[t.y][t.x - 1] > _costs[t.y][t.x] + 1 && map[t.y][t.x - 1] > 'Z')
                    {
                        _costs[t.y][t.x - 1] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.x - 1, t.y));
                    }
                    if (t.x < X - 1 && _costs[t.y][t.x + 1] > _costs[t.y][t.x] + 1 && map[t.y][t.x + 1] > 'Z')
                    {
                        _costs[t.y][t.x + 1] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.x + 1, t.y));
                    }
                }
                else
                {
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (result[i].x == t.x && result[i].y == t.y)
                        {
                            if (result[i].cost > _costs[t.y][t.x]) result[i] = (t.x, t.y, _costs[t.y][t.x]);
                            continue;
                        }
                    }

                    result.Add((t.x, t.y, _costs[t.y][t.x]));
                }
            }

            return result;
        }

        static bool IsEmpty(List<List<char>> map, int X, int Y)
        {
            for (int y = 0; y < Y; y++)
                for (int x = 0; x < X; x++)
                    if (map[y][x] >= 'a' && map[y][x] <= 'z')
                        return false;
            return true;
        }

        static (int x, int y) GetStart(List<List<char>> map, int X, int Y)
        {
            for (int y = 0; y < Y; y++)
                for (int x = 0; x < X; x++)
                    if (map[y][x] == '@')
                        return (x, y);
            throw new Exception();
        }

        static List<List<char>> Clone(List<List<char>> map)
        {
            return map.Select(l => l.ToList()).ToList();
        }

        private static int GetLettersHash()
        {
            var result = 0;
            for (int i = 'a'; i <= 'z'; i++)
            {
                var pos = i - 'a';
                if (_letterChecked[pos]) result += 1 << pos;
            }
            return result;
        }
    }
}
