﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace day18_2
{
    struct Pos
    {
        public int x1, x2, x3, x4, y1, y2, y3, y4;

        public Pos With(int x, int y, int no)
        {
            if (no == 1) return new Pos { x1 = x, x2 = x2, x3 = x3, x4 = x4, y1 = y, y2 = y2, y3 = y3, y4 = y4 };
            if (no == 2) return new Pos { x1 = x1, x2 = x, x3 = x3, x4 = x4, y1 = y1, y2 = y, y3 = y3, y4 = y4 };
            if (no == 3) return new Pos { x1 = x1, x2 = x2, x3 = x, x4 = x4, y1 = y1, y2 = y2, y3 = y, y4 = y4 };
            return new Pos { x1 = x1, x2 = x2, x3 = x3, x4 = x, y1 = y1, y2 = y2, y3 = y3, y4 = y };
        }
    }

    class Program
    {
        static List<List<int>> _costs;
        static Dictionary<(int hash, Pos pos), int> _checked = new Dictionary<(int, Pos), int>();
        static List<bool> _letterChecked = Enumerable.Repeat(false, 26).ToList();

        static void Main2()
        {
            // var input = System.IO.File.ReadAllLines("input2.txt");
            var input = System.IO.File.ReadAllLines("input2_8.txt");
            // var input = System.IO.File.ReadAllLines("input2_24.txt");
            // var input = System.IO.File.ReadAllLines("input2_72.txt");

            var map = input.Select(l => l.Select(c => c == '.' ? '|' : c).ToList()).ToList();
            var X = input[0].Length;
            var Y = input.Length;

            _costs = Enumerable.Range(0, Y).Select(_ => Enumerable.Repeat(int.MaxValue, X).ToList()).ToList();

            var pos = GetStart(map, X, Y);
            map[pos.y1][pos.x1] = '|';
            map[pos.y2][pos.x2] = '|';
            map[pos.y3][pos.x3] = '|';
            map[pos.y4][pos.x4] = '|';

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var steps = 0;
            var length = GetPathLength(map, X, Y, pos, 0, ref steps);
            stopwatch.Stop();

            Console.WriteLine("Result: " + length);
            Console.WriteLine("Steps:  " + steps);
            Console.WriteLine("Time:   " + stopwatch.ElapsedMilliseconds + "ms");
        }

        static int GetPathLength(List<List<char>> map, int X, int Y, Pos pos, int depth, ref int steps, int totalCost = 0)
        {
            if (IsEmpty(map, X, Y)) return 0;
            steps += 1;
            var min = int.MaxValue;
            var options = GetPossibilities(map, X, Y, pos);

            foreach (var v in options)
            {
                var bigLetter = map[v.yMoved][v.xMoved] - 32;
                for (int i = 'a'; i <= 'z'; i++) _letterChecked[i - 'a'] = true;

                (int x, int y)? toChange = null;
                for (int j = 0; j < Y; j++) for (int i = 0; i < X; i++)
                    {
                        if (map[j][i] == bigLetter) toChange = (i, j);
                        var letter = map[j][i];
                        if ((j != v.yMoved || i != v.xMoved) && letter >= 'a' && letter <= 'z') _letterChecked[letter - 'a'] = false;
                    }

                var key = (GetLettersHash(), v.pos);
                if (_letterChecked.Any(c => !c) && _checked.ContainsKey(key) && _checked[key] < totalCost + v.cost) continue;
                else
                {
                    if (!_checked.ContainsKey(key)) _checked.Add(key, totalCost + v.cost);
                    else _checked[key] = totalCost + v.cost;
                }

                var newMap = Clone(map);
                newMap[v.yMoved][v.xMoved] = '|';
                if (toChange.HasValue) newMap[toChange.Value.y][toChange.Value.x] = '|';

                var newMin = v.cost + GetPathLength(newMap, X, Y, v.pos, depth + 1, ref steps, totalCost + v.cost);
                if (newMin < min && newMin > 0) min = newMin;
            }

            return min;
        }

        private static IReadOnlyList<(Pos pos, int xMoved, int yMoved, int cost)> GetPossibilities(List<List<char>> map, int X, int Y, Pos pos)
        {
            var result = new List<(Pos pos, int xMoved, int yMoved, int cost)>();
            for (int j = 0; j < Y; j++) for (int i = 0; i < X; i++) _costs[j][i] = int.MaxValue;
            _costs[pos.y1][pos.x1] = 0;
            _costs[pos.y2][pos.x2] = 0;
            _costs[pos.y3][pos.x3] = 0;
            _costs[pos.y4][pos.x4] = 0;
            var toVisit = new Queue<(int no, int x, int y)>();
            toVisit.Enqueue((1, pos.x1, pos.y1));
            toVisit.Enqueue((2, pos.x2, pos.y2));
            toVisit.Enqueue((3, pos.x3, pos.y3));
            toVisit.Enqueue((4, pos.x4, pos.y4));
            while (toVisit.Count > 0)
            {
                var t = toVisit.Dequeue();
                var c = map[t.y][t.x];
                if (c == '|')
                {
                    if (t.y > 0 && _costs[t.y - 1][t.x] > _costs[t.y][t.x] + 1 && map[t.y - 1][t.x] > 'Z')
                    {
                        _costs[t.y - 1][t.x] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.no, t.x, t.y - 1));
                    }
                    if (t.y < Y - 1 && _costs[t.y + 1][t.x] > _costs[t.y][t.x] + 1 && map[t.y + 1][t.x] > 'Z')
                    {
                        _costs[t.y + 1][t.x] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.no, t.x, t.y + 1));
                    }
                    if (t.x > 0 && _costs[t.y][t.x - 1] > _costs[t.y][t.x] + 1 && map[t.y][t.x - 1] > 'Z')
                    {
                        _costs[t.y][t.x - 1] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.no, t.x - 1, t.y));
                    }
                    if (t.x < X - 1 && _costs[t.y][t.x + 1] > _costs[t.y][t.x] + 1 && map[t.y][t.x + 1] > 'Z')
                    {
                        _costs[t.y][t.x + 1] = _costs[t.y][t.x] + 1;
                        toVisit.Enqueue((t.no, t.x + 1, t.y));
                    }
                }
                else
                {
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (result[i].xMoved == t.x && result[i].yMoved == t.y)
                        {
                            if (result[i].cost > _costs[t.y][t.x]) result[i] = (pos.With(t.x, t.y, t.no), t.x, t.y, _costs[t.y][t.x]);
                            continue;
                        }
                    }

                    result.Add((pos.With(t.x, t.y, t.no), t.x, t.y, _costs[t.y][t.x]));
                }
            }

            return result;
        }

        static bool IsEmpty(List<List<char>> map, int X, int Y)
        {
            for (int y = 0; y < Y; y++)
                for (int x = 0; x < X; x++)
                    if (map[y][x] >= 'a' && map[y][x] <= 'z')
                        return false;
            return true;
        }

        static Pos GetStart(List<List<char>> map, int X, int Y)
        {
            var l = new List<(int x, int y)>(capacity: 4);
            for (int y = 0; y < Y; y++)
                for (int x = 0; x < X; x++)
                    if (map[y][x] == '@')
                        l.Add((x, y));
            return new Pos { x1 = l[0].x, x2 = l[1].x, x3 = l[2].x, x4 = l[3].x, y1 = l[0].y, y2 = l[1].y, y3 = l[2].y, y4 = l[3].y };
        }

        static List<List<char>> Clone(List<List<char>> map)
        {
            return map.Select(l => l.ToList()).ToList();
        }

        private static int GetLettersHash()
        {
            var result = 0;
            for (int i = 'a'; i <= 'z'; i++)
            {
                var pos = i - 'a';
                if (_letterChecked[pos]) result += 1 << pos;
            }
            return result;
        }
    }
}
