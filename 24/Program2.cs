﻿using System;
using System.Linq;

namespace _24_2
{
    class Program
    {
        static void Main()
        {
            var X = 5;
            var Y = 5;
            var D = 201;
            var S = 200;

            var boards = new bool[D][][];
            for (int d = 0; d < D; d++)
            {
                boards[d] = new bool[Y][];
                for (int y = 0; y < Y; y++) boards[d][y] = new bool[X];
            }
            boards[100] = System.IO.File.ReadLines("input.txt").Select(l => l.Select(c => c == '#').ToArray()).ToArray();

            for (int i = 0; i < S; i++)
            {
                var newBoards = new bool[D][][];
                for (int d = 0; d < D; d++)
                {
                    var newBoard = new bool[Y][];
                    for (int y = 0; y < Y; y++) newBoard[y] = new bool[X];

                    for (int y = 0; y < Y; y++)
                        for (int x = 0; x < X; x++)
                        {
                            var neighbors = 0;

                            if (x == 2 && y == 2) continue;

                            if (x > 0 && boards[d][y][x - 1]) neighbors += 1;
                            if (x < X - 1 && boards[d][y][x + 1]) neighbors += 1;
                            if (y > 0 && boards[d][y - 1][x]) neighbors += 1;
                            if (y < Y - 1 && boards[d][y + 1][x]) neighbors += 1;

                            if (d > 0)
                            {
                                if (x == 0 && boards[d - 1][2][1]) neighbors += 1;
                                if (x == 4 && boards[d - 1][2][3]) neighbors += 1;
                                if (y == 0 && boards[d - 1][1][2]) neighbors += 1;
                                if (y == 4 && boards[d - 1][3][2]) neighbors += 1;
                            }

                            if (d < D - 1)
                            {
                                if (y == 2 && x == 1) for (int yy = 0; yy < Y; yy++) if (boards[d + 1][yy][0]) neighbors += 1;
                                if (y == 2 && x == 3) for (int yy = 0; yy < Y; yy++) if (boards[d + 1][yy][4]) neighbors += 1;
                                if (y == 1 && x == 2) for (int xx = 0; xx < X; xx++) if (boards[d + 1][0][xx]) neighbors += 1;
                                if (y == 3 && x == 2) for (int xx = 0; xx < X; xx++) if (boards[d + 1][4][xx]) neighbors += 1;
                            }

                            newBoard[y][x] = boards[d][y][x] && neighbors != 1 ? false : !boards[d][y][x] && (neighbors == 1 || neighbors == 2) ? true : boards[d][y][x];
                        }

                    newBoards[d] = newBoard;
                }

                boards = newBoards;
            }

            var result = 0;
            for (int d = 0; d < D; d++)
                for (int y = 0; y < Y; y++)
                    for (int x = 0; x < X; x++)
                        if (boards[d][y][x]) result += 1;
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
