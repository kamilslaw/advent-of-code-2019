﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _24
{
    class Program
    {
        static void Main2()
        {
            var board = System.IO.File.ReadLines("input.txt").Select(l => l.Select(c => c == '#').ToArray()).ToArray();
            var X = board[0].Length;
            var Y = board.Length;
            var ratings = new HashSet<int>();

            while (true)
            {
                var rating = 0;
                for (int y = 0; y < Y; y++)
                    for (int x = 0; x < X; x++)
                        if (board[y][x])
                            rating += 1 << Y * y + x;

                for (int y = 0; y < Y; y++)
                {
                    for (int x = 0; x < X; x++) Console.Write(board[y][x] ? '#' : '.');
                    Console.WriteLine();
                }
                Console.WriteLine();

                if (ratings.Contains(rating))
                {
                    Console.WriteLine(rating);
                    break;
                }

                ratings.Add(rating);

                var newBoard = new bool[Y][];
                for (int y = 0; y < Y; y++) newBoard[y] = new bool[X];

                for (int y = 0; y < Y; y++)
                    for (int x = 0; x < X; x++)
                    {
                        var neighbors = 0;

                        if (x > 0 && board[y][x - 1]) neighbors += 1;
                        if (x < X - 1 && board[y][x + 1]) neighbors += 1;
                        if (y > 0 && board[y - 1][x]) neighbors += 1;
                        if (y < Y - 1 && board[y + 1][x]) neighbors += 1;

                        //if (x > 0 && y > 0 && board[y - 1][x - 1]) neighbors += 1;
                        //if (x < X - 1 && y < Y - 1 && board[y + 1][x + 1]) neighbors += 1;
                        //if (x > 0 && y < Y - 1 && board[y + 1][x - 1]) neighbors += 1;
                        //if (x < X - 1 && y > 0 && board[y - 1][x + 1]) neighbors += 1;

                        newBoard[y][x] = board[y][x] && neighbors != 1 ? false : !board[y][x] && (neighbors == 1 || neighbors == 2) ? true : board[y][x];
                    }

                board = newBoard;
            }

            Console.ReadKey();
        }
    }
}
