﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace day02
{
    class Program
    {
        static void Main(string[] args)
        {
            var codes = File.ReadAllText("input.txt").Split(',').Select(int.Parse).ToList();
            Part1(codes);
            codes = File.ReadAllText("input.txt").Split(',').Select(int.Parse).ToList();
            Part2(codes);
        }

        private static void Part1(List<int> codes)
        {
            codes[1] = 12;
            codes[2] = 2;

            var jumps = 0;
            var pos = 0;
            while (codes[pos] != 99 && jumps < 1_000_000)
            {
                if (codes[pos] == 1)
                {
                    codes[codes[pos + 3]] = codes[codes[pos + 1]] + codes[codes[pos + 2]];
                }
                else if (codes[pos] == 2)
                {
                    codes[codes[pos + 3]] = codes[codes[pos + 1]] * codes[codes[pos + 2]];
                }

                jumps += 1;
                pos += 4;
            }

            Console.WriteLine($"Jumps:  {jumps}");
            Console.WriteLine($"Result: {codes[0]}");
        }

        private static void Part2(List<int> codes)
        {
            var copy = codes.ToList();

            int n = 0;
            int v = 0;
            for (n = 0; n < 100; n++)
            {
                for (v = 0; v < 100; v++)
                {
                    for (int i = 0; i < copy.Count; i++) codes[i] = copy[i];
                    codes[1] = n;
                    codes[2] = v;
                    var pos = 0;
                    while (codes[pos] != 99)
                    {
                        if (codes[pos] == 1)
                            codes[codes[pos + 3]] = codes[codes[pos + 1]] + codes[codes[pos + 2]];
                        else if (codes[pos] == 2)
                            codes[codes[pos + 3]] = codes[codes[pos + 1]] * codes[codes[pos + 2]];
                        pos += 4;
                    }

                    if (codes[0] == 19_690_720) goto dd;
                }
            }

            dd:
            Console.WriteLine($"Result: {100 * n + v} (n: {n}, v: {v})");
        }
    }
}
