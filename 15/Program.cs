﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day15
{
    // north (1), south (2), west (3), and east (4)

    public enum Status
    {
        unknown = 0,
        wall = 1,
        path = 2,
    }

    public class Pos
    {
        public static Pos Wall = new Pos { cost = int.MinValue };

        public int cost; // maxValue
        public int prev; // None, U, R, D, L - -1, 1, 4, 2, 3
        public Status[] @checked; // U, R, D, L
    }

    class Program
    {
        static (int x, int y) Move(int x, int y, int dir)
        {
            switch (dir)
            {
                case 1: return (x, y - 1);
                case 4: return (x + 1, y);
                case 2: return (x, y + 1);
                case 3: return (x - 1, y);
                default: throw new Exception();
            }
        }

        static int ReverseDir(int dir)
        {
            switch (dir)
            {
                case 1: return 2;
                case 4: return 3;
                case 2: return 1;
                case 3: return 4;
                default: throw new Exception();
            }
        }

        static int GetMove(int x, int y, Pos pos, HashSet<(int x, int y)> visited)
        {
            for (int i = 1; i < 5; i++) if (pos.@checked[i] == Status.unknown && !visited.Contains(Move(x, y, i))) return i;
            return pos.prev;
        }

        // ----- TO RESOLVE WITH https://github.com/worm00111/Escape-The-Maze?fbclid=IwAR2WbPTjrmlnu9_1zkfDrok8DIX_G1qijxy1Oh_qU12rOkfAOKhKPZTOUys
        static void Draw(Dictionary<(int x, int y), Pos> locations, (int x, int y) currentLocation)
        {
            Console.WriteLine();
            for (int i = -21; i < 20; i++)
            {
                for (int j = -21; j < 20; j++)
                {
                    if (i == 00 && j == 0) Console.Write("S");
                    else if (!locations.ContainsKey((j, i))) Console.Write("#"); // " "
                    else if (i == currentLocation.y && j == currentLocation.x) Console.Write("G");
                    else if (locations[(j, i)] == Pos.Wall) Console.Write("#");
                    else Console.Write(" "); // "."
                }
                Console.WriteLine();
            }
        }

        static void Main()
        {
            var codes = System.IO.File.ReadAllText("input.txt")
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 10_000).Select(_ => 0L))
                .ToList();

            var visited = new HashSet<(int x, int y)> { (0, 0) };
            var locations = new Dictionary<(int, int), Pos> { [(0, 0)] = new Pos { cost = 0, @checked = new Status[5] } };
            (int x, int y) location = (0, 0);
            var lastDir = -1;
            (int x, int y) finalLocation = (int.MinValue, int.MaxValue);

            var pos = 0;
            var relativeBase = 0;

            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3 || codes[pos] == 203) // part 2
                {
                    var index = codes[pos] == 3 ? (int)codes[pos + 1] : (int)(relativeBase + codes[pos + 1]);
                    lastDir = GetMove(location.x, location.y, locations[location], visited);
                    if (lastDir == -1) break;
                    codes[index] = lastDir;
                    pos += 2;
                }
                else if (codes[pos] == 4 || codes[pos] == 104 || codes[pos] == 204)
                {
                    var output = codes[pos] == 4 ? codes[(int)codes[pos + 1]] : codes[pos] == 104 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];
                    pos += 2;

                    var newLocation = Move(location.x, location.y, lastDir);
                    visited.Add(newLocation);
                    if (output == 0)
                    {
                        locations[location].@checked[lastDir] = Status.wall;
                        if (!locations.ContainsKey(newLocation)) locations.Add(newLocation, Pos.Wall);
                    }
                    else
                    {
                        if (!locations.ContainsKey(newLocation)) locations.Add(newLocation, new Pos { cost = int.MaxValue, @checked = new Status[5] });
                        locations[newLocation].@checked[ReverseDir(lastDir)] = Status.path;
                        if (locations[location].prev != lastDir) locations[newLocation].prev = ReverseDir(lastDir);
                        locations[location].@checked[lastDir] = Status.path;
                        location = newLocation;

                        if (output == 2)
                        {
                            finalLocation = location;
                        }
                    }
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }

            Draw(locations, finalLocation);
            Console.WriteLine("Tries: " + visited.Count);
            var visitedCount = visited.Count(l => locations[l] != Pos.Wall);
            Console.WriteLine("Visited: " + visitedCount);

            //visited.Clear();
            //visited.Add((0, 0));

            //location = (0, 0);
            //var cost = 0;
            //while (visited.Count != visitedCount)
            //{

            //}
            // second part done with https://github.com/worm00111/Escape-The-Maze?fbclid=IwAR2WbPTjrmlnu9_1zkfDrok8DIX_G1qijxy1Oh_qU12rOkfAOKhKPZTOUys modified (print dijkstra cost & remove end point)
        }
    }
}
