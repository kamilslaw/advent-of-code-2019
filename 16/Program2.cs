﻿using System;
using System.Linq;

namespace day16_2
{
    class Program
    {
        static void Main()
        {
            var signal = System.IO.File.ReadAllText("input.txt").Select(c => c - 48).ToArray();
            var point = 5970221;
            //var signal = "03036732577212944063491565474664".Select(c => c - 48).ToArray(); // 0303673 - 84462026
            //var point = 303673;
            //var signal = "02935109699940807407585447034323".Select(c => c - 48).ToArray(); // 0293510 - 78725270
            //var point = 293510;
            //var signal = "03081770884921959731165446850517".Select(c => c - 48).ToArray(); // 0308177 - 53553731
            //var point = 308177;

            signal = Enumerable.Repeat(signal, 10_000).SelectMany(x => x).ToArray();
            signal = signal.Skip(point).ToArray();

            for (int i = 0; i < 101; i ++)
            {
                Console.WriteLine(string.Join("", signal.Take(8)));
                Reload(signal);
            }
        }

        static void Reload(int[] signal)
        {
            for (int i = signal.Length - 2; i >= 0; i--)
                signal[i] = (signal[i] + signal[i + 1]) % 10;
        }
    }
}
