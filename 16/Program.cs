﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day16
{
    class Program
    {
        static void Main2()
        {
            var pattern = new[] { 0, 1, 0, -1 };
            var signal = System.IO.File.ReadAllText("input.txt").Select(c => c - 48).ToList();
            // var signal = "80871224585914546619083218645595".Select(c => c - 48).ToList(); // 24176176
            // var signal = "19617804207202209144916044189917".Select(c => c - 48).ToList(); // 73745418
            // var signal = "69317163492948606335995924319873".Select(c => c - 48).ToList(); // 52432133
            // var signal = "12345678".Select(c => c - 48).ToList(); // 52432133

            for (int _ = 0; _ < 101; _++)
            {
                Console.WriteLine($"{_}: " + string.Join("", signal.Take(8)));
                signal = Enumerable.Range(1, signal.Count).Select(i => GetDigit(signal, pattern, i)).ToList();
            }
        }

        static int GetDigit(IReadOnlyList<int> signal, int[] pattern, int repeats)
        {
            var value = 0;
            var position = 0;
            var innerPosition = 1;
            for (int i = 0; i < signal.Count; i++)
            {
                if (innerPosition == repeats)
                {
                    innerPosition = 0;
                    position = position == pattern.Length - 1 ? 0 : position + 1;
                }

                value += signal[i] * pattern[position];
                innerPosition += 1;
            }

            return Math.Abs(value % 10);
        }
    }
}
