﻿using System;
using System.Linq;

namespace _21
{
    class Program
    {
        static void Main()
        {
            var codes = System.IO.File.ReadAllText("input.txt")
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 10_000).Select(_ => 0L))
                .ToList();

            var pos = 0;
            var relativeBase = 0;

            // var input = "NOT A J\nNOT C T\nAND D T\nOR T J\nWALK\n"; // PART 1 - if a is not ground, jump; if c is hole & d is ground, jump
            var input = "NOT B T\nNOT C J\nOR T J\nAND D J\nAND H J\nNOT A T\nOR T J\nRUN\n"; // PART 2 ((D AND H AND (NOT B OR NOT C)) OR (NOT A))
            var inputPos = 0;

            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3 || codes[pos] == 203)
                {
                    var index = codes[pos] == 3 ? (int)codes[pos + 1] : (int)(relativeBase + codes[pos + 1]);
                    codes[index] = input[inputPos++];
                    pos += 2;
                }
                else if (codes[pos] == 4 || codes[pos] == 104 || codes[pos] == 204)
                {
                    var result = codes[pos] == 4 ? codes[(int)codes[pos + 1]] : codes[pos] == 104 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];
                    pos += 2;
                    Console.Write(result < 127 ? ((char)result).ToString() : result.ToString());
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }

            Console.ReadKey();
        }
    }
}
