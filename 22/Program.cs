﻿using System;
using System.Linq;

namespace _22
{
    class Program
    {
        const int L = 10_007;

        static void Main2()
        {
            var instructions = System.IO.File.ReadAllLines("input.txt");
            var deck = Enumerable.Range(0, L).ToArray();
            foreach (var i in instructions)
            {
                var parts = i.Split(' ');
                if (parts[parts.Length - 1] == "stack") deck = Deal(deck);
                else if (parts[0]== "cut") deck = Cut(deck, int.Parse(parts[1]));
                else deck = DealWithIncrement(deck, int.Parse(parts[3]));
            }

            Console.WriteLine("2019 at index " + deck.ToList().IndexOf(2019));
            Console.ReadKey();
        }

        static int[] Deal(int[] deck)
        {
            for (int i = 0; i < L / 2; i++)
            {
                var temp = deck[i];
                deck[i] = deck[L - 1 - i];
                deck[L - 1 - i] = temp;
            }

            return deck;
        }

        static int[] Cut(int[] deck, int n)
        {
            var newDeck = new int[L];
            var index = 0;
            if (n > 0)
            {
                for (int i = n; i < L; i++) newDeck[index++] = deck[i];
                for (int i = 0; i < n; i++) newDeck[index++] = deck[i];
            }
            else
            {
                n = n * -1;
                for (int i = L - n; i < L; i++) newDeck[index++] = deck[i];
                for (int i = 0; i < L - n; i++) newDeck[index++] = deck[i];
            }

            return newDeck;
        }

        static int[] DealWithIncrement(int[] deck, int n)
        {
            var newDeck = new int[L];
            var index = 0;
            for (int i = 0; i < L; i++)
            {
                newDeck[index] = deck[i];
                index += n;
                index %= L;
            }

            return newDeck;
        }
    }
}
