﻿using System;
using System.Linq;

namespace _22_2
{
    // https://www.geeksforgeeks.org/multiplicative-inverse-under-modulo-m/
    // https://dev.to/jbristow/advent-of-code-2019-solution-megathread-day-22-slam-shuffle-5832
    class Program
    {
        // const long L = 20_011L; // liczba L musi być pierwsza, wtedy cykl powtarza się dokładnie w połowie (tutaj 10_005), czyli w 20_0010 iteracji też jest wartość 2020
        // - zmiana kolejności w pliku input.txt nie ma na to wpływu (ewentualnie występuje 20_010 lub 3335, czyli wciąż dzielniki 20_010

        const long L = 13L;
        // const long L = 10_007L;
        // const long L = 119_315_717_514_047;

        static void Main()
        {
            // var instructionsStr = System.IO.File.ReadAllLines("input.txt");
            var instructionsStr = System.IO.File.ReadAllLines("myInput.txt"); //.Take(1);
            var instructions = instructionsStr.Select<string, Func<long, long>>(instruction =>
            {
                var parts = instruction.Split(' ');
                if (parts[parts.Length - 1] == "stack") return i => Deal(i);
                else if (parts[0] == "cut")
                {
                    var n = int.Parse(parts[1]);
                    return i => Cut(n, i);
                }
                else
                {
                    var n = int.Parse(parts[3]);
                    return i => DealWithIncrement(n, i);
                }
            }).ToList();

            var reverseInstructions = instructionsStr.Reverse().Select<string, Func<long, long>>(instruction =>
            {
                var parts = instruction.Split(' ');
                if (parts[parts.Length - 1] == "stack") return i => DealReverse(i);
                else if (parts[0] == "cut")
                {
                    var n = int.Parse(parts[1]);
                    return i => CutReverse(n, i);
                }
                else
                {
                    var n = int.Parse(parts[3]);
                    return i => DealWithIncrementReverse(n, i);
                }
            }).ToList();

            //var index = 2019L;
            //foreach (var @in in instructions) index = @in(index);
            //Console.WriteLine(index); // For L == 10_007 should be 6289

            //var index = 2020L;
            //foreach (var @in in reverseInstructions) index = @in(index);
            //Console.WriteLine(index); // For L == 10_007 should be 8941
            //foreach (var @in in reverseInstructions) index = @in(index);
            //Console.WriteLine(index); // For L == 10_007 should be 5138
            //foreach (var @in in reverseInstructions) index = @in(index);
            //Console.WriteLine(index); // For L == 10_007 should be 5416

            var index = 8L;
            var r = 0;
            do
            {
               Console.WriteLine($"{r.ToString().PadLeft(3, ' ')}:{index.ToString().PadLeft(15, ' ')}");
                foreach (var @in in reverseInstructions) index = @in(index);
                r += 1;
            } while (r < 14);
        }

        static long DealReverse(long y) => L - 1 - y;

        static long DealWithIncrementReverse(int n, long y)
        {
            var d = 0;
            while (true)
            {
                var candidate = (y + d * L) / n;
                if (y == DealWithIncrement(n, candidate)) return candidate;
                d += 1;
            }
        }

        static long CutReverse(int n, long y)
        {
            if (n > 0)
            {
                var x1 = y - L + n;
                var x2 = y + n;
                return x1 >= 0 && x1 < L ? x1 : x2;
            }
            else
            {
                n = n * -1;
                var x1 = y - n;
                var x2 = y + (L - n);
                return x1 >= 0 && x1 < L ? x1 : x2;
            }
        }

        static long Deal(long x) => L - 1 - x;

        static long Cut(int n, long x)
        {
            if (n > 0) return x < n ? L + x - n : x - n;
            n = n * -1;
            return x < (L - n) ? x + n : x - (L - n);
        }

        static long DealWithIncrement(int n, long x) => x * n % L;
    }
}
