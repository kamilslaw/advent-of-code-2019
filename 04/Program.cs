﻿using System;
using System.Linq;

namespace day04
{
    class Program
    {
        static readonly (int from, int to) input = (248345, 746315);

        static void Main()
        {
            var result = Enumerable.Range(0, input.to + 1).Skip(input.from).Where(MeetPredicate).Count();
            Console.WriteLine(result);
            Console.ReadKey();
        }

        static bool MeetPredicate(int val)
        {
            var values = val.ToString().Select(c => c - 48).ToList();
            for (int i = 1; i < 6; i++)
                if (values[i] < values[i - 1]) return false;

            Span<bool> excluded = stackalloc bool[10];
            for (int i = 1; i < 6; i++)
                if (values[i] == values[i - 1] && !excluded[values[i]])
                {
                    var anySameLater = false;
                    for (int j = i + 1; j < 6; j++)
                        if (values[i] == values[j]) anySameLater = true;
                    if (!anySameLater) return true;
                    else excluded[values[i]] = true;
                }

            return false;
        }
    }
}
