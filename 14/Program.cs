﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day14
{
    public class SimpleEl
    {
        public int count;
        public string name;
    }

    public class El
    {
        public int count;
        public string name;
        public SimpleEl[] req;
    }

    public class Leaf
    {
        public List<Leaf> parents = new List<Leaf>();
        public List<Leaf> children = new List<Leaf>();
        public SimpleEl[] req;
        public string name;
        public int min;
        public int need;
        public bool visited;

        public override string ToString() => $"{name}|{min}|{need}";
    }

    class Program
    {
        static void Main2()
        {
            var input = Inputs.Parse(Inputs.str);
            var tree = BuildTree(input);
            tree["FUEL"].need = 1;
            while (!tree["ORE"].parents.All(p => p.visited))
            {
                var leaf = tree.Values.First(l => !l.visited && l.parents.All(p => p.visited));
                leaf.visited = true;
                var multiplier = (int)Math.Ceiling((decimal)leaf.need / leaf.min);
                foreach (var chLeaf in leaf.children)
                    chLeaf.need += leaf.req.Single(el => el.name == chLeaf.name).count * multiplier;
            }

            Console.WriteLine("Result: " + tree["ORE"].need);
        }

        private static Dictionary<string, Leaf> BuildTree(List<El> input)
        {
            var tree = new Dictionary<string, Leaf>();
            foreach (var el in input)
            {
                Leaf leaf = null;
                if (tree.ContainsKey(el.name))
                {
                    leaf = tree[el.name];
                    leaf.req = el.req;
                    leaf.min = el.count;
                }
                else
                {
                    leaf = new Leaf { name = el.name, min = el.count, req = el.req };
                    tree.Add(leaf.name, leaf);
                }

                foreach (var subEl in el.req)
                {
                    Leaf subLeaf = null;
                    if (tree.ContainsKey(subEl.name)) subLeaf = tree[subEl.name];
                    else
                    {
                        subLeaf = new Leaf { name = subEl.name };
                        tree.Add(subLeaf.name, subLeaf);
                    }

                    subLeaf.parents.Add(leaf);
                    leaf.children.Add(subLeaf);
                }
            }

            return tree;
        }
    }
}
