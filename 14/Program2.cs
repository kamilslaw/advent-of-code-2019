﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day14_2
{
    public class SimpleEl
    {
        public decimal count;
        public string name;
    }

    public class El
    {
        public string name;
        public SimpleEl[] req;
    }

    public class Leaf
    {
        public List<Leaf> parents = new List<Leaf>();
        public List<Leaf> children = new List<Leaf>();
        public SimpleEl[] req;
        public string name;
        public decimal need;
        public bool visited;

        public override string ToString() => $"{name}|{need}";
    }

    class Program
    {
        static void Main()
        {
            var input = day14.Inputs.Parse2(day14.Inputs.str);
            var tree = BuildTree(input);
            tree["FUEL"].need = 1;
            while (!tree["ORE"].parents.All(p => p.visited))
            {
                var leaf = tree.Values.First(l => !l.visited && l.parents.All(p => p.visited));
                leaf.visited = true;
                foreach (var chLeaf in leaf.children)
                {
                    chLeaf.need += leaf.req.Single(el => el.name == chLeaf.name).count * leaf.need;
                }
            }

            Console.WriteLine("Result: " + tree["ORE"].need);
            Console.WriteLine("Produce: " + ((1_000_000_000_000 / tree["ORE"].need) - 2).ToString("N2")); // righrt answer -2 !!! only for input, for test inputs is ok without -2
        }

        private static Dictionary<string, Leaf> BuildTree(List<El> input)
        {
            var tree = new Dictionary<string, Leaf>();
            foreach (var el in input)
            {
                Leaf leaf = null;
                if (tree.ContainsKey(el.name))
                {
                    leaf = tree[el.name];
                    leaf.req = el.req;
                }
                else
                {
                    leaf = new Leaf { name = el.name, req = el.req };
                    tree.Add(leaf.name, leaf);
                }

                foreach (var subEl in el.req)
                {
                    Leaf subLeaf = null;
                    if (tree.ContainsKey(subEl.name)) subLeaf = tree[subEl.name];
                    else
                    {
                        subLeaf = new Leaf { name = subEl.name };
                        tree.Add(subLeaf.name, subLeaf);
                    }

                    subLeaf.parents.Add(leaf);
                    leaf.children.Add(subLeaf);
                }
            }

            return tree;
        }
    }
}
