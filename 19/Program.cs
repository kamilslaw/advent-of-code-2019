﻿using System;
using System.Diagnostics;
using System.Linq;

namespace day19
{
    class Program
    {
        static void Main()
        {
            var SIZE = 2_500;

            var codes = System.IO.File.ReadAllText("input.txt")
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 50).Select(_ => 0L))
                .ToList();
            var copy = codes.ToList();

            var pos = 0;
            var relativeBase = 0;

            var values = Enumerable.Range(0, SIZE).Select(_ => new int[SIZE]).ToArray();
            var (x, y) = (0, 0);
            var nowX = true;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            while (true)
            {
                if (codes[pos] == 99)
                {
                    if (x == SIZE - 1 && y == SIZE - 1) break;

                    for (int i = 0; i < codes.Count; i++) codes[i] = copy[i];
                    pos = 0;
                    relativeBase = 0;
                    continue;
                }
                else if (codes[pos] == 3 || codes[pos] == 203)
                {
                    var index = codes[pos] == 3 ? (int)codes[pos + 1] : (int)(relativeBase + codes[pos + 1]);
                    codes[index] = nowX ? x : y;
                    nowX = !nowX;
                    pos += 2;
                }
                else if (codes[pos] == 4 || codes[pos] == 104 || codes[pos] == 204)
                {
                    var result = codes[pos] == 4 ? codes[(int)codes[pos + 1]] : codes[pos] == 104 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];
                    pos += 2;
                    values[y][x] = (int)result;
                    x += 1;
                    if (x == SIZE)
                    {
                        x = 0;
                        y += 1;
                    }
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }

            stopwatch.Stop();
            Console.WriteLine("Time: " + stopwatch.ElapsedMilliseconds + "ms");

            Console.WriteLine("Result: " + values.SelectMany(l => l).Count(v => v == 1) + " of " + SIZE * SIZE);

            for (y = 0; y < SIZE - 99; y++)
                for (x = 0; x < SIZE - 99; x++)
                {
                    if (values[y][x] == 1 && values[y + 99][x] == 1 && values[y][x + 99] == 1)
                    {
                        goto end;
                    }
                }
            end:
            Console.WriteLine(x);
            Console.WriteLine(y);
            Console.WriteLine(10_000 * x + y);
        }
    }
}
