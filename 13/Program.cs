﻿using System;
using System.Linq;

namespace day13
{
    public enum TileType
    {
        none = -1,
        empty = 0,
        wall = 1,
        block = 2,
        paddle = 3,
        ball = 4
    }

    class Program
    {
        static void Main()
        {
            var codes = System.IO.File.ReadAllText("input.txt")
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 10_000).Select(_ => 0L))
                .ToList();
            var grid = new TileType[22][];
            for (int i = 0; i < grid.Length; i++) grid[i] = Enumerable.Range(0, 38).Select(_ => TileType.none).ToArray();

            int? x = null;
            int? y = null;

            var pos = 0;
            var relativeBase = 0;

            codes[0] = 2; // part 2
            var score = 0; // part 2

            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3 || codes[pos] == 203) // part 2
                {
                    var index = codes[pos] == 3 ? (int)codes[pos + 1] : (int)(relativeBase + codes[pos + 1]);
                    codes[index] = GetMove(grid);
                    pos += 2;
                }
                else if (codes[pos] == 4 || codes[pos] == 104 || codes[pos] == 204)
                {
                    var output = codes[pos] == 4 ? codes[(int)codes[pos + 1]] : codes[pos] == 104 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];
                    pos += 2;
                    if (!x.HasValue)
                    {
                        x = (int)output;
                    }
                    else if (!y.HasValue)
                    {
                        y = (int)output;
                    }
                    else
                    {
                        if (x == -1) score = (int)output; // part 2
                        else grid[y.Value][x.Value] = (TileType)output;
                        x = y = null;
                    }
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }

            Draw(grid);
            Console.WriteLine("Result: " + grid.Select(line => line.Count(t => t == TileType.block)).Sum());
            Console.WriteLine("Score: " + score); // part 2
        }

        static long GetMove(TileType[][] grid)
        {
            var ballPos = -1;
            var padPos = -1;
            foreach (var line in grid)
            {
                for (int x = 0; x < line.Length; x++)
                {
                    if (line[x] == TileType.ball) ballPos = x;
                    else if (line[x] == TileType.paddle) padPos = x;
                }
            }

            return ballPos < padPos ? -1 : ballPos > padPos ? 1 : 0;
        }

        static void Draw(TileType[][] grid)
        {
            foreach (var line in grid)
            {
                foreach (var tile in line)
                {
                    Console.Write(tile == TileType.ball ? 'o' : tile == TileType.block ? '#' : tile == TileType.empty ? ' ' : tile == TileType.paddle ? '_' : tile == TileType.wall ? '█' : '`');
                }
                Console.WriteLine();
            }
        }
    }
}
