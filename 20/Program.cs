﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day20
{
    class Program
    {
        static void Main2()
        {
            var (maze, points) = Load(System.IO.File.ReadAllLines("input.txt"));
            // var (maze, points) = Load(System.IO.File.ReadAllLines("input_23.txt"));
            // var (maze, points) = Load(System.IO.File.ReadAllLines("input_58.txt"));

            var cost = Enumerable.Range(0, maze.Count).Select(_ => Enumerable.Repeat(int.MaxValue, maze[0].Count).ToList()).ToList();
            var toVisit = new Queue<(int x, int y)>();

            cost[points["AA"].y][points["AA"].x] = 0;
            toVisit.Enqueue(points["AA"]);

            var moves = new (int x, int y)[] { (0, -1), (0, 1), (1, 0), (-1, 0) };
            while (toVisit.Any())
            {
                var pos = toVisit.Dequeue();
                foreach (var move in moves)
                {
                    var currentCost = cost[pos.y][pos.x];
                    var x = pos.x + move.x;
                    var y = pos.y + move.y;
                    var c = maze[y][x];
                    if (c == "#") continue;
                    if (c == "." || c == "ZZ")
                    {
                        if (cost[y][x] > currentCost + 1)
                        {
                            cost[y][x] = currentCost + 1;
                            if (c != "ZZ") toVisit.Enqueue((x, y));
                        }
                    }
                    else if (c != "AA")
                    {
                        var newPos = points[c].x == x && points[c].y == y ? points[c + "_"] : points[c];
                        if (cost[newPos.y][newPos.x] > currentCost + 2)
                        {
                            cost[y][x] = currentCost + 1;
                            cost[newPos.y][newPos.x] = currentCost + 2;
                            toVisit.Enqueue(newPos);
                        }
                    }
                }
            }

            var zzPos = points["ZZ"];
            Console.WriteLine("Result: " + cost[zzPos.y][zzPos.x]);
            Console.ReadKey();

           // foreach (var l in maze) Console.WriteLine(string.Join("", l.Select(s => s.PadLeft(2)))); // Draw maze
           // foreach (var l in cost) Console.WriteLine(string.Join("", l.Select(c => c == int.MaxValue ? " -" : c.ToString().PadLeft(2)))); // Draw cost
        }

        static (List<List<string>> maze, Dictionary<string, (int x, int y)> points) Load(string[] input)
        {
            var maze = Enumerable.Range(0, input.Length + 1).Select(_ => Enumerable.Repeat("", input[0].Length + 1).ToList()).ToList();
            var points = new Dictionary<string, (int, int)>();

            for (int y = 0; y < input.Length; y++)
                for (int x = 0; x < input[0].Length; x++)
                    maze[y][x] = input[y][x] == ' ' ? "#" : input[y][x].ToString();

            for (int y = 0; y < input.Length - 1; y++)
                for (int x = 0; x < input[0].Length; x++)
                    if (maze[y][x].Length == 1 && maze[y][x][0] >= 'A' && maze[y][x][0] <= 'Z')
                    {
                        if (maze[y][x + 1].Length > 0 && maze[y][x + 1][0] >= 'A' && maze[y][x + 1][0] <= 'Z')
                        {
                            if (x > 0 && maze[y][x - 1] == ".")
                            {
                                var c = maze[y][x] + maze[y][x + 1];
                                maze[y][x - 1] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x - 1, y));
                                else points.Add(c, (x - 1, y));
                            }
                            else
                            {
                                var c = maze[y][x] + maze[y][x + 1];
                                maze[y][x + 2] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x + 2, y));
                                else points.Add(c, (x + 2, y));
                            }
                            maze[y][x + 1] = "#";
                            maze[y][x] = "#";
                        }
                        else
                        {
                            if (y > 0 && maze[y - 1][x] == ".")
                            {
                                var c = maze[y][x] + maze[y + 1][x];
                                maze[y - 1][x] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x, y - 1));
                                else points.Add(c, (x, y - 1));
                            }
                            else
                            {
                                var c = maze[y][x] + maze[y + 1][x];
                                maze[y + 2][x] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x, y + 2));
                                else points.Add(c, (x, y + 2));
                            }
                            maze[y + 1][x] = "#";
                            maze[y][x] = "#";
                        }
                    }

            return (maze, points);
        }
    }
}
