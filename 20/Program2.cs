﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day20_part2
{
    class Program
    {
        static void Main()
        {
            var MAX = 100;

            var (maze, points) = Load(System.IO.File.ReadAllLines("input.txt"));
            // var (maze, points) = Load(System.IO.File.ReadAllLines("input_23.txt")); // '26' for task 2
            // var (maze, points) = Load(System.IO.File.ReadAllLines("input2_396.txt"));

            var cost = Enumerable.Range(0, MAX).Select(_ => Enumerable.Range(0, maze.Count).Select(__ => Enumerable.Repeat(int.MaxValue, maze[0].Count).ToList()).ToList()).ToList();
            var toVisit = new Queue<(int x, int y, int level)>();

            var X = maze[0].Count;
            var Y = maze.Count;

            cost[0][points["AA"].y][points["AA"].x] = 0;
            toVisit.Enqueue((points["AA"].x, points["AA"].y, 0));

            var moves = new (int x, int y)[] { (0, -1), (0, 1), (1, 0), (-1, 0) };
            while (toVisit.Any())
            {
                var pos = toVisit.Dequeue();
                if (pos.level == MAX - 1) continue;
                foreach (var move in moves)
                {
                    var currentCost = cost[pos.level][pos.y][pos.x];
                    var x = pos.x + move.x;
                    var y = pos.y + move.y;
                    var c = maze[y][x];
                    if (c == "#") continue;
                    if (c == "ZZ" && pos.level > 0) continue;
                    if (c == "." || c == "ZZ")
                    {
                        if (cost[pos.level][y][x] > currentCost + 1)
                        {
                            cost[pos.level][y][x] = currentCost + 1;
                            if (c != "ZZ") toVisit.Enqueue((x, y, pos.level));
                        }
                    }
                    else if (c != "AA")
                    {
                        var newPos = points[c].x == x && points[c].y == y ? points[c + "_"] : points[c];
                        var inner = pos.x > 6 && pos.x < X - 6 && pos.y > 6 && pos.y < Y - 6;
                        var level = inner ? pos.level + 1 : pos.level - 1;
                        if ((inner || pos.level > 0) && cost[pos.level][y][x] > currentCost + 1 && cost[level][newPos.y][newPos.x] > currentCost + 2)
                        {
                            cost[pos.level][y][x] = currentCost + 1;
                            cost[level][newPos.y][newPos.x] = currentCost + 2;
                            toVisit.Enqueue((newPos.x, newPos.y, level));
                        }
                    }
                }
            }

            var zzPos = points["ZZ"];
            var min = cost[0][zzPos.y][zzPos.x];
            Console.WriteLine("Result: " + min);

            //foreach (var l in maze) Console.WriteLine(string.Join("", l.Select(s => s.PadLeft(3)))); // Draw maze
            //for (int l = 0; l < 11; l++)
            //{
            //    Console.WriteLine();
            //    Console.WriteLine("Level " + l);
            //    for (int y = 0; y < Y; y++)
            //    {
            //        Console.WriteLine();
            //        for (int x = 0; x < X; x++)
            //        {
            //            if (maze[y][x] != ".") Console.Write(maze[y][x].PadLeft(3));
            //            else
            //            {
            //                var val = cost[l][y][x];
            //                Console.Write(val == int.MaxValue ? "  -" : val.ToString().PadLeft(3));
            //            }
            //        }
            //    }
            //}
        }

        static (List<List<string>> maze, Dictionary<string, (int x, int y)> points) Load(string[] input)
        {
            var maze = Enumerable.Range(0, input.Length + 1).Select(_ => Enumerable.Repeat("", input[0].Length + 1).ToList()).ToList();
            var points = new Dictionary<string, (int, int)>();

            for (int y = 0; y < input.Length; y++)
                for (int x = 0; x < input[0].Length; x++)
                    maze[y][x] = input[y][x] == ' ' ? "#" : input[y][x].ToString();

            for (int y = 0; y < input.Length - 1; y++)
                for (int x = 0; x < input[0].Length; x++)
                    if (maze[y][x].Length == 1 && maze[y][x][0] >= 'A' && maze[y][x][0] <= 'Z')
                    {
                        if (maze[y][x + 1].Length > 0 && maze[y][x + 1][0] >= 'A' && maze[y][x + 1][0] <= 'Z')
                        {
                            if (x > 0 && maze[y][x - 1] == ".")
                            {
                                var c = maze[y][x] + maze[y][x + 1];
                                maze[y][x - 1] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x - 1, y));
                                else points.Add(c, (x - 1, y));
                            }
                            else
                            {
                                var c = maze[y][x] + maze[y][x + 1];
                                maze[y][x + 2] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x + 2, y));
                                else points.Add(c, (x + 2, y));
                            }
                            maze[y][x + 1] = "#";
                            maze[y][x] = "#";
                        }
                        else
                        {
                            if (y > 0 && maze[y - 1][x] == ".")
                            {
                                var c = maze[y][x] + maze[y + 1][x];
                                maze[y - 1][x] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x, y - 1));
                                else points.Add(c, (x, y - 1));
                            }
                            else
                            {
                                var c = maze[y][x] + maze[y + 1][x];
                                maze[y + 2][x] = c;
                                if (points.ContainsKey(c)) points.Add(c + "_", (x, y + 2));
                                else points.Add(c, (x, y + 2));
                            }
                            maze[y + 1][x] = "#";
                            maze[y][x] = "#";
                        }
                    }

            return (maze, points);
        }
    }
}
