﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day03
{
    struct Point
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public int Value => Math.Abs(X) + Math.Abs(Y);

        public int Distance(Point other) => Math.Abs(X - other.X) + Math.Abs(Y - other.Y);

        public override string ToString() => $"{X},{Y}";
    }

    struct Line
    {
        public Line(Point p1, Point p2)
        {
            P1 = new[] { p1, p2 }.OrderBy(p => p.X).ThenBy(p => p.Y).First();
            P2 = new[] { p1, p2 }.OrderBy(p => p.X).ThenBy(p => p.Y).Last();
            First = p1;
        }

        public Point P1 { get; }
        public Point P2 { get; }
        public Point First { get; }

        public int Length => IsVertical ? P2.Y - P1.Y : P2.X - P1.X;

        public bool IsVertical => P1.X == P2.X;

        public Point? Cross(Line other)
        {
            if (!(IsVertical ^ other.IsVertical)) return null;

            var h = IsVertical ? other : this;
            var v = IsVertical ? this : other;

            if (v.P1.X < h.P1.X || v.P1.X > h.P2.X || h.P1.Y < v.P1.Y || h.P1.Y > v.P2.Y)
                return null;

            return new Point(v.P1.X, h.P1.Y);
        }

        public override string ToString() => $"({P1})->({P2})";
    }

    class Program
    {
        static void Main(string[] args)
        {
            var operations = System.IO.File.ReadAllLines("input.txt").Select(l => l.Split(',').ToList()).ToList();
            //var operations = new[] { "R8,U5,L5,D3", "U7,R6,D4,L4" }.Select(l => l.Split(',').ToList()).ToList();
            //var operations = new[] { "R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83" }.Select(l => l.Split(',').ToList()).ToList();
            //var operations = new[] { "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7" }.Select(l => l.Split(',').ToList()).ToList();

            var lines1 = GetLines(operations[0]);
            var lines2 = GetLines(operations[1]);
            var distance = GetDistance(lines1, lines2);
            Console.WriteLine("Result 1: " + distance);

            distance = GetDistanceByPath(lines1, lines2);
            Console.WriteLine("Result 2: " + distance);

            Console.ReadKey();
        }

        static IReadOnlyList<Line> GetLines(IReadOnlyList<string> operation)
        {
            var lines = new List<Line>(operation.Count);
            var point = new Point(0, 0);
            foreach (var step in operation)
            {
                var value = int.Parse(step.Substring(1));
                var newPoint = default(Point);
                if (step[0] == 'R')
                    newPoint = new Point(point.X + value, point.Y);
                else if (step[0] == 'L')
                    newPoint = new Point(point.X - value, point.Y);
                else if (step[0] == 'U')
                    newPoint = new Point(point.X, point.Y + value);
                else if (step[0] == 'D')
                    newPoint = new Point(point.X, point.Y - value);
                lines.Add(new Line(point, newPoint));
                point = newPoint;
            }

            return lines;
        }

        static int GetDistance(IReadOnlyList<Line> lines1, IReadOnlyList<Line> lines2)
        {
            var result = int.MaxValue;
            foreach (var l1 in lines1)
                foreach (var l2 in lines2)
                {
                    var crossPoint = l1.Cross(l2);
                    if (crossPoint.HasValue && crossPoint.Value.Value < result && crossPoint.Value.Value > 0) result = crossPoint.Value.Value;
                }

            return result;
        }

        static int GetDistanceByPath(IReadOnlyList<Line> lines1, IReadOnlyList<Line> lines2)
        {
            var result = int.MaxValue;
            for (int i = 0; i < lines1.Count; i++)
                for (int j = 0; j < lines2.Count; j++)
                {
                    var crossPoint = lines1[i].Cross(lines2[j]);
                    if (crossPoint.HasValue &&crossPoint.Value.Value > 0)
                    {
                        var distance = lines1.Take(i).Sum(l => l.Length) + lines1[i].First.Distance(crossPoint.Value)
                                     + lines2.Take(j).Sum(l => l.Length) + lines2[j].First.Distance(crossPoint.Value);
                        if (distance < result) result = distance;
                    }
                }

            return result;
        }
    }
}
