﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day07
{
    class Program2
    {
        static void Main()
        {
            var codes =
                System.IO.File.ReadAllText("input.txt")
                //"3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"
                //"3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"
                .Split(',').Select(int.Parse).ToList();

            var max = int.MinValue;
            var maxSeq = "00000";

            for (int a = 5; a < 10; a++)
                for (int b = 5; b < 10; b++)
                    for (int c = 5; c < 10; c++)
                        for (int d = 5; d < 10; d++)
                            for (int e = 5; e < 10; e++)
                            {
                                if (!(a != b && a != c && a != d && a != e && b != c && b != d && b != e && c != d && c != e && d != e))
                                    continue;

                                var c1 = codes.ToList();
                                var c2 = codes.ToList();
                                var c3 = codes.ToList();
                                var c4 = codes.ToList();
                                var c5 = codes.ToList();

                                var firstRun = true;
                                var pos = new[] { 0, 0, 0, 0, 0 };
                                var halt = false;
                                var outputs = new[] { 0, 0, 0, 0, 0 };

                                do
                                {
                                    Perform(c1, firstRun, a, outputs[0], ref outputs[1], ref pos[0]);
                                    Perform(c2, firstRun, b, outputs[1], ref outputs[2], ref pos[1]);
                                    Perform(c3, firstRun, c, outputs[2], ref outputs[3], ref pos[2]);
                                    Perform(c4, firstRun, d, outputs[3], ref outputs[4], ref pos[3]);
                                    halt = Perform(c5, firstRun, e, outputs[4], ref outputs[0], ref pos[4]);
                                    firstRun = false;
                                }
                                while (!halt);

                                if (outputs[0] > max)
                                {
                                    max = outputs[0];
                                    maxSeq = "" + a + b + c + d + e;
                                }
                            }

            Console.WriteLine("Result: " + max);
            Console.WriteLine("Seq:    " + maxSeq);
        }

        private static bool Perform(List<int> codes, bool firstRun, int firstInput, int input, ref int output, ref int pos)
        {
            while (true)
            {
                if (codes[pos] == 99)
                {
                    return true;
                }
                else if (codes[pos] == 3)
                {
                    codes[codes[pos + 1]] = firstRun ? firstInput : input;
                    pos += 2;
                    firstRun = false;
                }
                else if (codes[pos] == 4)
                {
                    output = codes[codes[pos + 1]];
                    pos += 2;
                    return false;
                }
                else if (codes[pos] == 104)
                {
                    output = codes[pos + 1];
                    pos += 2;
                    return false;
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var arg1 = op.Length < 3 || op[op.Length - 3] == '0' ? codes[codes[pos + 1]] : codes[pos + 1];
                    var arg2 = op.Length < 4 || op[op.Length - 4] == '0' ? codes[codes[pos + 2]] : codes[pos + 2];

                    if (last == '1')
                    {
                        codes[codes[pos + 3]] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[codes[pos + 3]] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '5')
                    {
                        if (arg1 != 0) pos = arg2;
                        else pos += 3;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = arg2;
                        else pos += 3;
                    }
                    else if (last == '7')
                    {
                        codes[codes[pos + 3]] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[codes[pos + 3]] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }
        }
    }
}
