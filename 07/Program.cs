﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day07
{
    class Program
    {
        static void MainTask1()
        {
            var codes =
                System.IO.File.ReadAllText("input.txt")
                //"3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
                //"3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
                //"3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"
                .Split(',').Select(int.Parse).ToList();

            var max = int.MinValue;
            var maxSeq = "00000";
            
            for (int a = 0; a < 5; a++)
                for (int b = 0; b < 5; b++)
                    for (int c = 0; c < 5; c++)
                        for (int d = 0; d < 5; d++)
                            for (int e = 0; e < 5; e++)
                            {
                                if (!(a != b && a != c && a != d && a != e && b != c && b != d && b != e && c != d && c != e && d != e))
                                    continue;
                                var output = 0;
                                Perform(codes.ToList(), a, ref output);
                                Perform(codes.ToList(), b, ref output);
                                Perform(codes.ToList(), c, ref output);
                                Perform(codes.ToList(), d, ref output);
                                Perform(codes.ToList(), e, ref output);
                                if (output > max)
                                {
                                    max = output;
                                    maxSeq = "" + a + b + c + d + e;
                                }
                            }

            Console.WriteLine("Result: " + max);
            Console.WriteLine("Seq:    " + maxSeq);
        }

        private static void Perform(List<int> codes, int firstOutput, ref int output)
        {
            var wasFirstOutputUsed = false;
            var pos = 0;
            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3)
                {
                    codes[codes[pos + 1]] = wasFirstOutputUsed ? output : firstOutput;
                    wasFirstOutputUsed = true;
                    pos += 2;
                }
                else if (codes[pos] == 4)
                {
                    output = codes[codes[pos + 1]];
                    Console.WriteLine(" > " + output);
                    pos += 2;
                }
                else if (codes[pos] == 104)
                {
                    output = codes[pos + 1];
                    Console.WriteLine(" > " + output);
                    pos += 2;
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var arg1 = op.Length < 3 || op[op.Length - 3] == '0' ? codes[codes[pos + 1]] : codes[pos + 1];
                    var arg2 = op.Length < 4 || op[op.Length - 4] == '0' ? codes[codes[pos + 2]] : codes[pos + 2];

                    if (last == '1')
                    {
                        codes[codes[pos + 3]] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[codes[pos + 3]] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '5')
                    {
                        if (arg1 != 0) pos = arg2;
                        else pos += 3;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = arg2;
                        else pos += 3;
                    }
                    else if (last == '7')
                    {
                        codes[codes[pos + 3]] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[codes[pos + 3]] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }
        }
    }
}
