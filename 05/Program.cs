﻿using System;
using System.Linq;

namespace day05
{
    class Program
    {
        static void Main()
        {
            var codes =
                System.IO.File.ReadAllText("input.txt")
                // "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
                .Split(',').Select(int.Parse).ToList();

            var output = 5;
            var pos = 0;

            while (true)
            {
               // Console.WriteLine(codes[pos]);
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3)
                {
                    codes[codes[pos + 1]] = output;
                    pos += 2;
                }
                else if (codes[pos] == 4)
                {
                    output = codes[codes[pos + 1]];
                    Console.WriteLine(" > " + output);
                    pos += 2;
                }
                else if (codes[pos] == 104)
                {
                    output = codes[pos + 1];
                    Console.WriteLine(" > " + output);
                    pos += 2;
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var arg1 = op.Length < 3 || op[op.Length - 3] == '0' ? codes[codes[pos + 1]] : codes[pos + 1];
                    var arg2 = op.Length < 4 || op[op.Length - 4] == '0' ? codes[codes[pos + 2]] : codes[pos + 2];

                    if (last == '1')
                    {
                        codes[codes[pos + 3]] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[codes[pos + 3]] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '5')
                    {
                        if (arg1 != 0) pos = arg2;
                        else pos += 3;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = arg2;
                        else pos += 3;
                    }
                    else if (last == '7')
                    {
                        codes[codes[pos + 3]] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[codes[pos + 3]] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }
        }
    }
}
