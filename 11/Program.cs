﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day11
{
    public enum Color
    {
        B,
        W
    }

    public enum Dir
    {
        U,
        D,
        L,
        R
    }

    public struct Pos
    {
        public int x { get; set; }
        public int y { get; set; }
        public Dir d { get; set; }

        public Pos Move(Dir dir)
        {
            if (dir != Dir.L && dir != Dir.R) throw new Exception();            
            if (d == Dir.U)
            {
                if (dir == Dir.L)
                    return new Pos { d = Dir.L, x = x - 1, y = y };
                else
                    return new Pos { d = Dir.R, x = x + 1, y = y };
            }
            else if (d == Dir.D)
            {
                if (dir == Dir.L)
                    return new Pos { d = Dir.R, x = x + 1, y = y };
                else
                    return new Pos { d = Dir.L, x = x - 1, y = y };
            }
            else if (d == Dir.R)
            {
                if (dir == Dir.L)
                    return new Pos { d = Dir.U, x = x, y = y - 1 };
                else
                    return new Pos { d = Dir.D, x = x, y = y + 1 };
            }
            else
            {
                if (dir == Dir.L)
                    return new Pos { d = Dir.D, x = x, y = y + 1 };
                else
                    return new Pos { d = Dir.U, x = x, y = y - 1 };
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var codes = System.IO.File.ReadAllText("input.txt")
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 10_000).Select(_ => 0L))
                .ToList();

            var colors = new Dictionary<(int x, int y), Color>
            {
                [(0, 0)] = Color.W
            };

            var robot = new Pos { d = Dir.U, x = 0, y = 0 };
            var pos = 0;
            var relativeBase = 0;

            var readColor = true; // color / move
            var nextColor = Color.B;

            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3 || codes[pos] == 203)
                {
                    var key = (robot.x, robot.y);
                    var index = codes[pos] == 3 ? (int)codes[pos + 1] : (int)(relativeBase + codes[pos + 1]);
                    codes[index] = !colors.ContainsKey(key) || colors[key] == Color.B ? 0 : 1;
                    pos += 2;
                }
                else if (codes[pos] == 4 || codes[pos] == 104 || codes[pos] == 204)
                {
                    var output = codes[pos] == 4 ? codes[(int)codes[pos + 1]] : codes[pos] == 104 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];
                    if (readColor)
                    {
                        nextColor = output == 0 ? Color.B : Color.W;
                    }
                    else
                    {
                        var key = (robot.x, robot.y);
                        if (colors.ContainsKey(key)) colors[key] = nextColor;
                        else colors.Add(key, nextColor);
                        robot = robot.Move(output == 0 ? Dir.L : Dir.R);
                    }
                    pos += 2;
                    readColor = !readColor;
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }

            Console.WriteLine("Visited points: " + colors.Count);
            var maxX = colors.Keys.Max(k => k.x);
            var maxY = colors.Keys.Max(k => k.y);
            for (int j = 0; j <= maxY; j++)
            {
                for (int i = 0; i <= maxX; i++)
                {
                    if (!colors.ContainsKey((i, j))) Console.Write("X");
                    else Console.Write(colors[(i, j)] == Color.B ? "#" : " ");
                }

                Console.WriteLine();
            }
        }
    }
}
