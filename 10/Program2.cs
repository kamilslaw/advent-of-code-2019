﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day10.t2
{
    public enum El
    {
        None,
        Aster
    }

    class Program2
    {
        static void Main()
        {
            t1.Program1.MainTask1();
            var filename =
                "input.txt";
                // "input_11_13_210.txt";
            var input = System.IO.File.ReadAllLines(filename).Select(line => line.Select(c => c == '.' ? El.None : El.Aster).ToArray()).ToArray();

            var i = 1;
            var x = 22;
            var y = 28;
            while (i < 300)
            {
                var lastQuarter = -1;
                var lastAngle = -1M;

                var asteroids = new List<(int quarter, decimal angle, int distance, int x, int y)>();
                for (int ty = 0; ty < input.Length; ty++)
                {
                    for (int tx = 0; tx < input[0].Length; tx++)
                    {
                        if ((ty != y || tx != x) && input[ty][tx] == El.Aster)
                            asteroids.Add(GetRay(x, y, tx, ty));
                    }
                }

                asteroids = asteroids.OrderBy(a => a.quarter).ThenBy(a => a.angle).ThenBy(a => a.distance).ToList();
                foreach (var a in asteroids)
                {
                    if (lastQuarter == a.quarter && lastAngle == a.angle)
                    {
                        // Console.WriteLine($"Asteroid ({a.x},{a.y}) saved in {i}th round");
                    }
                    else
                    {
                        lastQuarter = a.quarter;
                        lastAngle = a.angle;
                        input[a.y][a.x] = El.None;
                        Console.WriteLine($"Asteroid ({a.x},{a.y}) destroyed in {i}th round");
                        i += 1;
                    }
                }
            }
        }

        static (int quarter, decimal angle, int distance, int x, int y) GetRay(int x, int y, int tx, int ty)
        {
            var quarter = (tx >= x && ty < y)
                        ? 1
                        : (tx <= x && ty > y)
                        ? 3
                        : (tx > x && ty >= y)
                        ? 2
                        : 4;

            var angle = quarter == 1 || quarter == 3
                ? (decimal)Math.Abs(x - tx) / Math.Abs(y - ty)
                : (decimal)Math.Abs(y - ty) / Math.Abs(x - tx);

            var distance = Math.Abs(x - tx) + Math.Abs(y - ty);

            return (quarter, angle, distance, tx, ty);
        }
    }
}
