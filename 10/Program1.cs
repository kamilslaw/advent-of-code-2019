﻿using System;
using System.Linq;

namespace day10.t1
{
    public enum El
    {
        None,
        Aster
    }

    class Program1
    {
        public static void MainTask1()
        {
            try
            {
                var filename =
                    "input.txt";
                    //"input_1_2_35.txt";
                    //"input_11_13_210.txt";
                    // "input_5_8_33.txt";
                    //"input_6_3_41.txt";
                    //"input_3_4_8.txt";
                var input = System.IO.File.ReadAllLines(filename).Select(line => line.Select(c => c == '.' ? El.None : El.Aster).ToArray()).ToArray();

                var max = (-1, -1, -1);
                for (int y = 0; y < input.Length; y++)
                {
                    for (int x = 0; x < input[0].Length; x++)
                    {
                        var count = ReachedCount(input, x, y);
                        if (count > max.Item3) max = (x, y, count);
                        Console.Write(count.ToString().PadLeft(4));
                    }

                    Console.WriteLine();
                }

                Console.WriteLine(max);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex);
            }
        }

        static int ReachedCount(El[][] input, int px, int py)
        {
            if (input[py][px] == El.None) return 0;

            var result = 0;
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[0].Length; x++)
                {
                    if ((px == x && py == y) || input[y][x] == El.None)
                    {
                        continue;
                    }

                    var dx = Math.Abs(px - x);
                    var dy = Math.Abs(py - y);
                    var max = Math.Max(dx, dy);
                    var min = Math.Min(dx, dy);
                    var angle = min == 0 ? 0 : (decimal)max / min;

                    if (min != 0 && NWD(max, min) == 1)
                    {
                        result += 1;
                    }
                    else
                    {
                        var collision = false;
                        if (angle == 0 && py == y)
                        {
                            for (int i = Math.Min(px, x) + 1; i < Math.Max(px, x); i++)
                                if (input[py][i] == El.Aster) collision = true;
                        }
                        else if (angle == 0 && px == x)
                        {
                            for (int j = Math.Min(py, y) + 1; j < Math.Max(py, y); j++)
                                if (input[j][px] == El.Aster) collision = true;
                        }
                        else
                        {
                            for (int j = Math.Min(py, y) + 1; j < Math.Max(py, y); j++)
                                for (int i = Math.Min(px, x) + 1; i < Math.Max(px, x); i++)
                                {
                                    var interDx = Math.Abs(px - i);
                                    var interDy = Math.Abs(py - j);
                                    var interMax = Math.Max(interDx, interDy);
                                    var interMin = Math.Min(interDx, interDy);
                                    var interAngle = interMin == 0 ? 0 : (decimal)interMax / interMin;
                                    if (interAngle == angle
                                        && input[j][i] == El.Aster
                                        && interDx / interDy == dx / dy) collision = true;
                                }
                        }

                        if (!collision) result += 1;
                    }
                }
            }

            return result;
        }

        static int NWD(int a, int b)
        {
            if (b == 0) return a;
            return NWD(b, (a % b));
        }
    }
}
