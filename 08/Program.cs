﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day08
{
    class Program
    {
        static void Main()
        {
            var pixels = System.IO.File.ReadAllText("input.txt").Trim().Select(c => c - 48).ToList();
            var width = 6;
            var height = 25;
            var layers = pixels.Batch(width * height).Select(l => l.ToList()).ToList();

            // Task 1
            var targetLayer = layers.OrderBy(l => l.Count(p => p == 0)).First();
            Console.WriteLine("Result " + (targetLayer.Count(p => p == 1) * targetLayer.Count(p => p == 2)));

            // Task 2
            for (int w = 0; w < width; w++)
            {
                for (int h = 0; h < height; h++)
                {
                    for (int l = 0; l < layers.Count; l++)
                    {
                        var pixel = layers[l][height * w + h];
                        if (pixel == 0) Console.Write("▓");
                        else if (pixel == 1) Console.Write(" ");
                        if (pixel != 2) break;
                    }
                }
                Console.WriteLine();
            }
        }
    }

    static class MyLinqExtensions
    {
        public static IEnumerable<IEnumerable<T>> Batch<T>(
            this IEnumerable<T> source, int batchSize)
        {
            using (var enumerator = source.GetEnumerator())
                while (enumerator.MoveNext())
                    yield return YieldBatchElements(enumerator, batchSize - 1);
        }

        private static IEnumerable<T> YieldBatchElements<T>(
            IEnumerator<T> source, int batchSize)
        {
            yield return source.Current;
            for (int i = 0; i < batchSize && source.MoveNext(); i++)
                yield return source.Current;
        }
    }

}
