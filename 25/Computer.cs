﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _25
{
    class Computer
    {
        public static void Loop(List<long> codes, Func<long> getInput, Action<long> proceedOutput)
        {
            var pos = 0;
            var relativeBase = 0;

            while (true)
            {
                if (codes[pos] == 99)
                {
                    break;
                }
                else if (codes[pos] == 3 || codes[pos] == 203)
                {
                    var index = codes[pos] == 3 ? (int)codes[pos + 1] : (int)(relativeBase + codes[pos + 1]);
                    codes[index] = getInput();
                    pos += 2;
                }
                else if (codes[pos] == 4 || codes[pos] == 104 || codes[pos] == 204)
                {
                    var result = codes[pos] == 4 ? codes[(int)codes[pos + 1]] : codes[pos] == 104 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];
                    pos += 2;
                    proceedOutput(result);
                }
                else
                {
                    var op = codes[pos].ToString();
                    var last = op.Last();

                    var mod1 = op.Length < 3 || op[op.Length - 3] == '0' ? 0 : op[op.Length - 3] == '1' ? 1 : 2;
                    var arg1 = mod1 == 0 ? codes[(int)codes[pos + 1]] : mod1 == 1 ? codes[pos + 1] : codes[(int)(relativeBase + codes[pos + 1])];

                    if (last == '9')
                    {
                        relativeBase += (int)arg1;
                        pos += 2;
                        continue;
                    }

                    var mod2 = op.Length < 4 || op[op.Length - 4] == '0' ? 0 : op[op.Length - 4] == '1' ? 1 : 2;
                    var arg2 = mod2 == 0 ? codes[(int)codes[pos + 2]] : mod2 == 1 ? codes[pos + 2] : codes[(int)(relativeBase + codes[pos + 2])];

                    if (last == '5')
                    {
                        if (arg1 != 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }
                    else if (last == '6')
                    {
                        if (arg1 == 0) pos = (int)arg2;
                        else pos += 3;
                        continue;
                    }

                    var mod3 = op.Length < 5 || op[op.Length - 5] == '0' ? 0 : op[op.Length - 5] == '1' ? 1 : 2;
                    var target = mod3 == 1 ? throw new ArgumentException() : mod3 == 0 ? (int)codes[pos + 3] : (int)(relativeBase + codes[pos + 3]);

                    if (last == '1')
                    {
                        codes[target] = arg1 + arg2;
                        pos += 4;
                    }
                    else if (last == '2')
                    {
                        codes[target] = arg1 * arg2;
                        pos += 4;
                    }
                    else if (last == '7')
                    {
                        codes[target] = arg1 < arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else if (last == '8')
                    {
                        codes[target] = arg1 == arg2 ? 1 : 0;
                        pos += 4;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }
        }
    }
}
