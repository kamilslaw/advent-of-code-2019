﻿using System;
using System.Linq;

namespace _25
{
    class Program
    {
        static void Main()
        {
            var codes = System.IO.File.ReadAllText("input.txt")
                .Split(',')
                .Select(long.Parse)
                .Concat(Enumerable.Range(0, 1_000).Select(_ => 0L))
                .ToList();

            var output = string.Empty;
            var input = string.Empty;
            var inputPos = 0;

            Computer.Loop(codes,
                () =>
                {
                    if (string.IsNullOrEmpty(input)) input = Console.ReadLine() + '\n';
                    return input[inputPos++];
                },
                val =>
                {
                    input = null;
                    inputPos = 0;

                    if (val == 10)
                    {
                        Console.WriteLine(output);
                        output = string.Empty;
                    }
                    else output += (char)val;
                });
        }
    }
}
