﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace day12
{
    class Vector
    {
        public int x, y, z;

        public Vector(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override string ToString() => $"<x={x}, y={y}, z={z}>";
    }

    class Moon
    {
        public Vector pos, vel;

        public Moon(Vector pos, Vector vel)
        {
            this.pos = pos;
            this.vel = vel;
        }

        public void ApplyMove()
        {
            pos.x += vel.x;
            pos.y += vel.y;
            pos.z += vel.z;
        }

        public static Moon Parse(string str)
        {
            var elements = str.Replace(" ", "").Replace("x", "").Replace("y", "").Replace("z", "").Replace("=", "").Trim('<', '>').Split(',');
            return new Moon(new Vector(int.Parse(elements[0]), int.Parse(elements[1]), int.Parse(elements[2])), new Vector(0, 0, 0));
        }

        public override string ToString() => $"pos={pos}, vel={vel}";
    }

    class Program
    {
        static void Main2()
        {
            List<Moon> moons = System.IO.File.ReadLines("input.txt").Select(Moon.Parse).ToList();
            //List<Moon> moons = System.IO.File.ReadLines("inputTest.txt").Select(Moon.Parse).ToList();
            for (int i = 0; i < 1000; i++)
            {
               // Console.WriteLine($"After {i} steps:");
              //  moons.ForEach(m => Console.WriteLine(m));
                moons = PerformStep(moons);
            }

            Console.WriteLine("Result:");
            Console.WriteLine(moons.Sum(m => (Math.Abs(m.pos.x)+ Math.Abs(m.pos.y) + Math.Abs(m.pos.z)) * (Math.Abs(m.vel.x) + Math.Abs(m.vel.y) + Math.Abs(m.vel.z))));
            //moons.ToList().ForEach(m => Console.WriteLine(m));
        }

        static List<Moon> PerformStep(IReadOnlyList<Moon> moons)
        {
            var result = moons.Select(m =>
            {
                var vel = new Vector(
                    m.vel.x + moons.Sum(r => Diff(m.pos.x, r.pos.x)),
                    m.vel.y + moons.Sum(r => Diff(m.pos.y, r.pos.y)),
                    m.vel.z + moons.Sum(r => Diff(m.pos.z, r.pos.z)));
                return new Moon(m.pos, vel);
            }).ToList();

            result.ForEach(m => m.ApplyMove());
            return result;
        }

        static int Diff(int a, int b)
        {
            return a < b ? 1 : a > b ? -1 : 0;
        }
    }
}
