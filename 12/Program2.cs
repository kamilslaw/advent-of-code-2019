﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace day12_2
{
    class Program2
    {
        class Vector
        {
            public int x, y, z;

            public int this[int i] => i == 0 ? x : i == 1 ? y:z;

            public Vector(int x, int y, int z)
            {
                this.x = x;
                this.y = y;
                this.z = z;
            }

            public override string ToString() => $"<x={x}, y={y}, z={z}>";
        }

        class Moon
        {
            public Vector pos, vel;

            public Moon(Vector pos, Vector vel)
            {
                this.pos = pos;
                this.vel = vel;
            }

            public void ApplyMove()
            {
                pos.x += vel.x;
                pos.y += vel.y;
                pos.z += vel.z;
            }

            public static Moon Parse(string str)
            {
                var elements = str.Replace(" ", "").Replace("x", "").Replace("y", "").Replace("z", "").Replace("=", "").Trim('<', '>').Split(',');
                return new Moon(new Vector(int.Parse(elements[0]), int.Parse(elements[1]), int.Parse(elements[2])), new Vector(0, 0, 0));
            }

            public override string ToString() => $"pos={pos}, vel={vel}";

            public bool Equals(Moon other, int i)
            {
                return other.pos[i] == pos[i] && other.vel[i] == vel[i];
            }
        }

        class Program
        {
            static void Main()
            {
                List<Moon> init = System.IO.File.ReadLines("input.txt").Select(Moon.Parse).ToList();
                List<Moon> moons = System.IO.File.ReadLines("input.txt").Select(Moon.Parse).ToList();
                var i = 0;

                while (i < 1_000_000)
                {
                    moons = PerformStep(moons);
                    //if (moons[0].Equals(init[0], 0) && moons[1].Equals(init[1], 0) && moons[2].Equals(init[2], 0) && moons[3].Equals(init[3], 0))
                    //     Console.WriteLine("x: " + i); // each 186028
                    //  if (moons[0].Equals(init[0], 1) && moons[1].Equals(init[1], 1) && moons[2].Equals(init[2], 1) && moons[3].Equals(init[3], 1))
                    //      Console.WriteLine("y: " + i); // each 161428
                    //if (moons[0].Equals(init[0], 2) && moons[1].Equals(init[1], 2) && moons[2].Equals(init[2], 2) && moons[3].Equals(init[3], 2))
                    //     Console.WriteLine("z: " + i); // each 167624
                    // LCM(186028, 161428, 167624) = 314610635824376
                    i += 1;
                }
            }

            static List<Moon> PerformStep(IReadOnlyList<Moon> moons)
            {
                var result = moons.Select(m =>
                {
                    var vel = new Vector(
                        m.vel.x + moons.Sum(r => Diff(m.pos.x, r.pos.x)),
                        m.vel.y + moons.Sum(r => Diff(m.pos.y, r.pos.y)),
                        m.vel.z + moons.Sum(r => Diff(m.pos.z, r.pos.z)));
                    return new Moon(m.pos, vel);
                }).ToList();

                result.ForEach(m => m.ApplyMove());
                return result;
            }

            static int Diff(int a, int b)
            {
                return a < b ? 1 : a > b ? -1 : 0;
            }
        }
    }
}
